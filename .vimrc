set shell=bash\ -i

" Vundle
  filetype off
  set rtp+=~/.vim/bundle/Vundle.vim
  call vundle#begin()
  Plugin 'VundleVim/Vundle.vim'
  Plugin 'scrooloose/nerdtree'

  Plugin 'tomtom/tcomment_vim'
  Plugin 'tpope/vim-fugitive'
  Plugin 'ervandew/supertab'
  Plugin 'junegunn/vim-easy-align'
  Plugin 'ntpeters/vim-better-whitespace'
  Plugin 'traviswsims/vim-straight-quotes'
  Plugin 'vim-scripts/AnsiEsc.vim'
  Plugin 'bling/vim-airline'
  Plugin 'airblade/vim-gitgutter'
  Bundle 'Shougo/neocomplete.vim'
  Bundle 'wesQ3/vim-windowswap'

  Plugin 'flazz/vim-colorschemes'
  Bundle 'uarun/vim-protobuf'

  " Languages
  Plugin 'kchmck/vim-coffee-script'
  Plugin '2072/PHP-Indenting-for-VIm'
  Plugin 'vim-scripts/Flex-4'
  Plugin 'cakebaker/scss-syntax.vim'
  Plugin 'hail2u/vim-css3-syntax'

  Plugin 'tpope/vim-haml'
  Plugin 'heartsentwined/vim-emblem'
  Plugin 'slim-template/vim-slim.git'
  Plugin 'elzr/vim-json'

  Plugin 'guns/vim-clojure-static'
  Plugin 'luochen1990/rainbow'

  Plugin 'traviswsims/xoria256-darker'

  Plugin 'jremmen/vim-ripgrep'
  Plugin 'junegunn/fzf'

  Plugin 'lervag/vimtex'
  call vundle#end()

syntax on
filetype plugin indent on

set laststatus=2 " powerline
set guifont=FiraCode-Retina:h15
set linespace=7

set cryptmethod=blowfish2

set cursorline
set nocompatible " lots of vimrc bugs otherwise
set vb " disable Vim bell
set ruler " show line and column number at the bottom of the screen
set nowrap
set autoindent
set tabstop=2
set shiftwidth=2
set expandtab
set number
set ic " ignore search case
set hls " highlight search matches
set nofoldenable " disable folding
set lazyredraw
set regexpengine=1
set cpoptions+=x " stay at matching search result after Esc
set ttyfast

nmap K i<CR><Esc>

" Start interactive EasyAlign in visual mode
vmap <Enter> <Plug>(EasyAlign)

" Start interactive EasyAlign with a Vim movement
nmap <Leader>a <Plug>(EasyAlign)

map <Leader>e :NERDTreeToggle <CR> :set nu <CR>
map <D-k> :TComment <CR>
map <F1> <Esc>
map! <F1> <Esc>

" File types
au BufNewFile,BufRead,BufEnter *.sass set ft=sass
au Bufread,BufNewFile *.as   set ft=actionscript
au BufRead,BufNewFile *.mxml set ft=actionscript
au BufRead,BufNewFile *.rabl set ft=ruby
au BufRead,BufNewFile *.edn  set ft=clojure
au BufRead,BufNewFile *.em   set ft=emblem
au BufNewFile,BufRead,BufEnter *.md set ft=markdown
au BufRead,BufNewFile *.md setlocal textwidth=120
au BufRead,BufNewFile *.tex setlocal textwidth=120
au BufRead,BufNewFile Podfile set filetype=ruby


" Don't use working location as vim's dumping ground
set backupdir=~/.vim/backup//
set directory=~/.vim/swap//
set undodir=~/.vim/undo//

" Remap to switch tabs
if has("gui_macvim")
  " Press Ctrl-Tab to switch between open tabs (like browser tabs) to
  " the right side. Ctrl-Shift-Tab goes the other way.
  noremap <C-Tab> :tabnext<CR>
  noremap <C-S-Tab> :tabprev<CR>

  " Switch to specific tab numbers with Command-number
  noremap <D-1> :tabn 1<CR>
  noremap <D-2> :tabn 2<CR>
  noremap <D-3> :tabn 3<CR>
  noremap <D-4> :tabn 4<CR>
  noremap <D-5> :tabn 5<CR>
  noremap <D-6> :tabn 6<CR>
  noremap <D-7> :tabn 7<CR>
  noremap <D-8> :tabn 8<CR>
  noremap <D-9> :tabn 9<CR>
  " Command-0 goes to the last tab
  noremap <D-0> :tablast<CR>
endif


" Remap to switch buffers more naturally
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

set splitbelow
set splitright

let g:rg_derive_root=1
let g:rg_highlight=1
noremap <Leader>r :Rg<Space>
noremap <Leader>f :FZF .<CR>
noremap <Leader>fs :FZF src/<CR>
noremap <Leader>F :FZF<Space>

set showtabline=2
let g:vim_json_syntax_conceal = 0

colorscheme xoria256-darker

let g:rainbow_active = 1
